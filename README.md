## Role Player Game 
Basic back end system of an RPG that can create characters, equip them with armor and weapons, level up and show their progress. 

The game is controlled via a menu where one can pick or create a character. When a character is chosen, the menu will allow the user to equip its character with armor and weapon. When chosing equip gear, a set of gear is created matching the level the character is currently in. There is three different types of armor and weapon. One type to fit each character type the best. The plate matches warrior class, the leather ranger and cloth the mage. The character will only do damage when a weapon is equipped. 

When gear is equipped the already equipped gear if any will automaticly go into the inventory. At this point the inventory can be shown but not interacted with.  