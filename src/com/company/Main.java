package com.company;

import com.company.Action.Level;
import com.company.Character.*;
import com.company.Character.Character;
import com.company.Item.Armor.Armor;
import com.company.Item.Armor.ArmorSlot;
import com.company.Item.Armor.Material.Cloth;
import com.company.Item.Armor.Material.Leather;
import com.company.Item.Armor.Material.Plate;
import com.company.Item.Item;
import com.company.Item.Weapon.Type.Magic;
import com.company.Item.Weapon.Type.Melee;
import com.company.Item.Weapon.Type.Ranged;
import com.company.Item.Weapon.Weapon;

import java.util.*;


public class Main {

    private final HashMap<String,Character> characterMap = new HashMap<>();
    private String primaryCharacterKey = "";
    private static final Scanner inputScanner = new Scanner(System.in);

    public Main(){
        //Creates 1 of each of the 3 character types and goes to start menu
        CharacterFactory factory = new CharacterFactory();
        Warrior warrior = (Warrior) factory.getCharacter(CharacterTypes.Warrior, "Warrior");
        Ranger ranger = (Ranger) factory.getCharacter(CharacterTypes.Ranger,"Ranger");
        Mage mage = (Mage) factory.getCharacter(CharacterTypes.Mage,"Mage");
        characterMap.put(warrior.getName(),warrior);
        characterMap.put(ranger.getName(),ranger);
        characterMap.put(mage.getName(),mage);
        startMenu();

    }



    public static void main(String[] args) {
        new Main();
    }

    public static void formatText(int len, String input) {
        String padded = String.format("%-" + (len - 1) + "s", input);
        System.out.println(padded + "|");
    }


    public void startMenu(){
        // The start menu will let you chose between creating and picking a character
        String topBot = "+***********************************+";
        System.out.println(topBot);
        formatText(topBot.length(),"| 1. Create Character");
        formatText(topBot.length(),"| 2. Choose Characters");
        formatText(topBot.length(),"| 9. Exit");
        System.out.println(topBot);

        String input = getInput();

        switch (input.toLowerCase().strip()) {
            case "1" -> createCharacterMenu();
            case "2" -> choseCharacterMenu();
            case "3", "9", "exit" -> System.exit(0);
            default -> startMenu();
        }

    }

    public void createCharacterMenu(){
        // Character creating menu
        String topBot = "+***********************************+";
        System.out.println(topBot);
        formatText(topBot.length(),"| 1. Create a Warrior");
        formatText(topBot.length(),"| 2. Create a Ranger");
        formatText(topBot.length(),"| 3. Create a Mage");
        formatText(topBot.length(),"| 9. Back");
        System.out.println(topBot);


        CharacterFactory factory = new CharacterFactory();
        Character character = null;
        String input = getInput();

        switch (input.toLowerCase().strip()){
            case "1":
                character = factory.getCharacter(CharacterTypes.Warrior, "Warrior");
                break;
            case "2":
                character = factory.getCharacter(CharacterTypes.Ranger,"Ranger");
                break;
            case "3":
                character = factory.getCharacter(CharacterTypes.Mage, "");
                break;
            case "4":
            case "9":
                startMenu();
            case "exit":
                System.exit(0);
                break;
            default:
                createCharacterMenu();
                return;
        }

        System.out.println("Please enter a name for your character");
        input = getInput();
        String name = input;
        boolean choice = true;

        while(choice){
            System.out.println("You have chosen the name : " + input);
            System.out.println("Press y to confirm or n to change");

            input = getInput();
            if(input.equals("y")){
                choice = false;
            }else{
                System.out.println("Please enter a new name");
                input = getInput();
                name = input;
            }

        }

        character.setName(name);
        character.printStats();
        characterMap.put(name,character);
        startMenu();
    }

    public void choseCharacterMenu(){
        // The menu will print all the current character in the hashmap and let you chose from it
        HashMap<String,String> tempMap = new HashMap<>();
        if(characterMap.isEmpty()){
            System.out.println("No characters has been created - returning to main menu");
            startMenu();
            return;
        }
        int index = 1;


        for(String key : characterMap.keySet()){
            tempMap.put(Integer.toString(index),key);
            System.out.println(index+".");
            characterMap.get(key).printStats();
            index++;
        }
        System.out.println("Enter index to chose your character");
        String input = getInput();

        if(tempMap.containsKey(input)){
            primaryCharacterKey = tempMap.get(input);
            characterActionMenu();
        }else{
            choseCharacterMenu();
        }



    }

    public void characterActionMenu(){
        // This menu shows your characters available actions
        Character character = characterMap.get(primaryCharacterKey);

        String topBot = "+***********************************+";
        System.out.println(topBot);
        formatText(topBot.length(),"| 1. Print Stats");
        formatText(topBot.length(),"| 2. Equip Gear");
        formatText(topBot.length(),"| 3. Unequip Gear");
        formatText(topBot.length(),"| 4. Show Inventory");
        formatText(topBot.length(),"| 5. Attack");
        formatText(topBot.length(),"| 6. Gain Level");
        formatText(topBot.length(),"| 9. Back");
        System.out.println(topBot);

        String input = getInput();

        switch (input.toLowerCase().strip()){
            case "1":
                character.printStats();
                break;
            case "2":
                equipArmor();
                break;
            case "3":
                unequipArmor();
                break;
            case "4":
                printInventory(character.getInventory().inventory);
                break;
            case "5":
                System.out.println(primaryCharacterKey + " does " + character.getDamage() + " damage.");
                characterActionMenu();
                break;
            case "6":
                gainXp();
                characterActionMenu();
                break;
            case "9":
            case "back":
                startMenu();
            case "exit":
                System.exit(0);
                break;
            default:
                characterActionMenu();
        }

        characterActionMenu();
    }

    public void equipArmor(){
        // Helper method to equip armor
        String name = characterMap.get(primaryCharacterKey).getClass().getSimpleName();
        int level = characterMap.get(primaryCharacterKey).getLevelInstance().getLevel();
        Character character = characterMap.get(primaryCharacterKey);

        Item weapon;
        Item helmet;
        Item body;
        Item legs;

        switch (name) {
            case "Warrior" -> {
                weapon = new Weapon("Axe of the giants", level, new Melee());
                helmet = new Armor("Helmet of the giants", new Plate(), level, ArmorSlot.HEAD);
                body = new Armor("Armor of the giants", new Plate(), level, ArmorSlot.BODY);
                legs = new Armor("Pants of the giants", new Plate(), level, ArmorSlot.LEGS);
            }
            case "Ranger" -> {
                weapon = new Weapon("Bow of the winds", level, new Ranged());
                helmet = new Armor("Helmet of the winds", new Leather(), level, ArmorSlot.HEAD);
                body = new Armor("Body of the winds", new Leather(), level, ArmorSlot.BODY);
                legs = new Armor("Pants of the winds", new Leather(), level, ArmorSlot.LEGS);
            }
            case "Mage" -> {
                weapon = new Weapon("Staff of the elders", level, new Magic());
                helmet = new Armor("Hood of the elders", new Cloth(), level, ArmorSlot.HEAD);
                body = new Armor("Robe of the elders", new Cloth(), level, ArmorSlot.BODY);
                legs = new Armor("Pants of the elders", new Cloth(), level, ArmorSlot.LEGS);
            }
            default -> {
                System.out.println("Error - returning to main menu");
                startMenu();
                return;
            }
        }
        weapon.equip(weapon,character);
        helmet.equip(helmet,character);
        body.equip(body,character);
        legs.equip(legs,character);

    }

    public void unequipArmor(){
        // Helper method to un equip armor
        Character character = characterMap.get(primaryCharacterKey);
        Item weapon = character.getInventory().getHand();
        Item helmet = character.getInventory().getHead();
        Item body = character.getInventory().getBody();
        Item legs = character.getInventory().getLegs();

        weapon.unequip(weapon,character);
        helmet.unequip(helmet,character);
        body.unequip(body,character);
        legs.unequip(legs,character);
    }

    public void printInventory(HashMap<String,Item> inventory){
        // Helper method to print inventory of the current character

        if(inventory.isEmpty()){
            System.out.println("Inventory is currently empty - Chose equip armor to get armor");
            characterActionMenu();
            return;
        }

        int index = 1;
        Item item;
        for(String key : inventory.keySet()){
            item = inventory.get(key);
            System.out.println(index);
            String topBot = "+***********************************+";
            System.out.println(topBot);
            formatText(topBot.length(),"| Name  : " + item.getName());
            formatText(topBot.length(),"| Type  : "+ item.getClass().getSimpleName());
            formatText(topBot.length(),"| Level : "+ item.getLevel());
            System.out.println(topBot);
            index++;
        }
    }

    public void gainXp(){
        // Will increase the level of the chosen character by 1 by getting current and target experience

        Level level = characterMap.get(primaryCharacterKey).getLevelInstance();
        int xp = (level.getTargetXP() - level.getCurrentXP())+1;

        level.gainXP(xp);

        while(level.checkXP()){
            level.gainLevel();
            characterMap.get(primaryCharacterKey).updateStats();
            characterMap.get(primaryCharacterKey).printStats();
        }

    }

    protected static String getInput(){
        String input = "";
        try{
            input = inputScanner.nextLine();
        }catch (Exception e){
            System.out.println("Error, please enter input again");
        }
        return input.toLowerCase().strip();
    }




}
