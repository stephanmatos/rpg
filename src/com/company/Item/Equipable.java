package com.company.Item;


import com.company.Character.Character;

public interface Equipable {

    void equip(Item item, Character character);

    void swapItem(Item item, Character character);

    void unequip(Item item, Character character);
}
