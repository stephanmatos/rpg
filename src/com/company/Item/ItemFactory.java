package com.company.Item;


import com.company.Item.Armor.Armor;
import com.company.Item.Armor.ArmorSlot;
import com.company.Item.Armor.Material.Material;

public class ItemFactory {
    // Armor factory - Didnt have time to use it for anything smart
    public Item getArmor(String name,Material material, int level, ArmorSlot slot) {
        if (slot == ArmorSlot.HEAD) {
            return new Armor(name, material, level, ArmorSlot.HEAD);
        } else if (slot == ArmorSlot.BODY) {
            return new Armor(name, material, level, ArmorSlot.BODY);
        } else if ( slot == ArmorSlot.LEGS){
            return new Armor(name, material, level, ArmorSlot.LEGS);
        }
        return null;
    }

}
