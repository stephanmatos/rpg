package com.company.Item.Armor;

import com.company.Action.Inventory;
import com.company.Character.Character;
import com.company.Item.Item;
import com.company.Item.Armor.Material.Material;
import com.company.Attributes;

import static com.company.Main.formatText;

public class Armor extends Item {

    private Material material;
    private final ArmorSlot slot;
    private final Attributes attributes;

    public Armor(String name, Material material, int level, ArmorSlot slot) {
        super(name, level);

        this.material = material;
        this.slot = slot;
        this.attributes = material.getAttributes(level,slot);

        attributes.setHealth(attributes.getHealth());
        attributes.setStrength(attributes.getStrength());
        attributes.setDexterity(attributes.getDexterity());
        attributes.setIntelligence(attributes.getIntelligence());

    }

    public void printArmorStats(){

        String first = "+***********************************+";
        System.out.println(first);
        formatText(first.length(),"| "+ super.getName() + " details:");
        formatText(first.length(),"| Armor Type : " + material.getClass().getSimpleName());
        formatText(first.length(),"| Armor Slot : " + slot);
        formatText(first.length(),"| HP         : " + attributes.getHealth());
        formatText(first.length(),"| Str        : " + attributes.getStrength());
        formatText(first.length(),"| Dex        : " + attributes.getDexterity());
        formatText(first.length(),"| Int        : " + attributes.getIntelligence());
        formatText(first.length(),"| Lvl        : " + getLevel());
        System.out.println("+***********************************+");

    }



    // Equip will equip the item on the character passed to the method.
    // It will check for level, and run the swap method that will put the current armor worn, if any, in the bag.
    @Override
    public void equip(Item item, Character character) {

        if(character.getLevelInstance().getLevel() >= item.getLevel()){
            swapItem(item, character);
            Attributes attributes = character.getAttributes();
            Inventory inventory = character.getInventory();

            ArmorSlot slot = ((Armor) item).getSlot();

            switch (slot) {
                case HEAD -> inventory.setHead(item);
                case BODY -> inventory.setBody(item);
                case LEGS -> inventory.setLegs(item);
            }
            attributes.addAllAttributes(((Armor) item).getAttributesInstance().getAllAttributes());
            inventory.getItemFromInventory(item.getName());

        }else{
            System.out.println("The item is to high level");
        }
        character.updateDamage();
    }

    @Override
    public void swapItem(Item item ,Character character) {
        Inventory inventory = character.getInventory();
        ArmorSlot slot = ((Armor) item).getSlot();
        Item swapItem = null;
        switch (slot) {
            case HEAD -> {
                swapItem = inventory.getHead();
                inventory.setHead(null);
            }
            case BODY -> {
                swapItem = inventory.getBody();
                inventory.setBody(null);
            }
            case LEGS -> {
                swapItem = inventory.getLegs();
                inventory.setLegs(null);
            }
        }

        if(swapItem != null){
            inventory.putItemInInventory(item.getName(), item);
            Attributes attributes = character.getAttributes();
            attributes.subtractAllAttributes(((Armor) item).getAttributesInstance().getAllAttributes());
        }
    }

    // unequip removed the item from the character if the character is wearing it. The item will be stored in the
    // inventory
    @Override
    public void unequip(Item item, Character character) {


        Inventory inventory = character.getInventory();

        if(item.equals(inventory.getHead())){
            inventory.setHead(null);
        }else if(item.equals(inventory.getBody())){
            inventory.setBody(null);
        }else if(item.equals(inventory.getLegs())){
            inventory.setLegs(null);
        }else{
            return;
        }

        inventory.putItemInInventory(item.getName(), item);

        Attributes attributes = character.getAttributes();
        attributes.subtractAllAttributes(((Armor) item).getAttributesInstance().getAllAttributes());
        character.updateDamage();
    }

    public Attributes getAttributesInstance(){
        return this.attributes;
    }
    public ArmorSlot getSlot(){
        return slot;
    }
}
