package com.company.Item.Armor.Material;

import com.company.Item.Armor.ArmorSlot;
import com.company.Attributes;

public interface Material {

    Attributes getAttributes(int level, ArmorSlot slot);

}
