package com.company.Item.Armor.Material;

import com.company.Item.Armor.ArmorSlot;
import com.company.Attributes;

public class Leather implements Material {

    @Override
    public Attributes getAttributes(int level, ArmorSlot slot) {
        double bonus = 1;

        if(slot == ArmorSlot.HEAD){
            bonus = 0.8;
        }else if(slot == ArmorSlot.LEGS){
            bonus = 0.6;
        }

        int healthPoints = (int) ((20 + 8 * level) * bonus);
        int strength = (int) ( (1 + level) * bonus);
        int dexterity = (int) ( (3 + 2 * level) * bonus);
        int intelligence = 0;

        Attributes attributes = new Attributes(healthPoints,strength,dexterity,0);

        return attributes;
    }
}
