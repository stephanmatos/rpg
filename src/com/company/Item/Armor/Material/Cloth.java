package com.company.Item.Armor.Material;

import com.company.Item.Armor.ArmorSlot;
import com.company.Attributes;

public class Cloth implements Material{

    @Override
    public Attributes getAttributes(int level, ArmorSlot slot) {

        double bonus = 1;
        if(slot == ArmorSlot.HEAD){
            bonus = 0.8;
        }else if(slot == ArmorSlot.LEGS){
            bonus = 0.6;
        }

        int healthPoints = (int) ((10 + 5 * level) * bonus);
        int dexterity = (int) ((1 + level) * bonus);
        int intelligence = (int) ((3 + level * 2) * bonus);

        Attributes attributes = new Attributes(healthPoints,0,dexterity,intelligence);

        return attributes;
    }
}
