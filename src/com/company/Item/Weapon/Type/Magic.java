package com.company.Item.Weapon.Type;

import com.company.Attributes;

public class Magic implements WeaponType{

    // Returns the base and bonus damage of the magic weapon

    @Override
    public int getFullDamage(int level, Attributes attributes) {
        return 25 + 2 * level + 3 * attributes.getIntelligence();
    }

    @Override
    public int getBaseDamage(int level) {
        return 25 + 2 * level;
    }
}
