package com.company.Item.Weapon.Type;

import com.company.Attributes;

public interface WeaponType {

    int getFullDamage(int level, Attributes attributes);
    int getBaseDamage(int level);

}
