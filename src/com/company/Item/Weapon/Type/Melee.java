package com.company.Item.Weapon.Type;

import com.company.Attributes;

public class Melee implements WeaponType{

    // Returns the base and bonus damage of the melee weapon
    @Override
    public int getFullDamage(int level, Attributes attributes) {
        return (int) (15 + 2 * level + 1.5 * attributes.getStrength());
    }

    @Override
    public int getBaseDamage(int level) {
        return 15 + 2 * level;
    }
}
