package com.company.Item.Weapon.Type;

import com.company.Attributes;

public class Ranged implements WeaponType{

    // Returns the base and bonus damage of the ranged weapon
    @Override
    public int getFullDamage(int level, Attributes attributes) {
        return 5 + 3 * level + 2 * attributes.getDexterity();
    }

    @Override
    public int getBaseDamage(int level) {
        return 5 + 3 * level;
    }
}
