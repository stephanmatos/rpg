package com.company.Item.Weapon;


import com.company.Action.Inventory;
import com.company.Character.Character;
import com.company.Item.Item;
import com.company.Item.Weapon.Type.WeaponType;

public class Weapon extends Item {

    private final WeaponType weaponType;

    public Weapon(String name, int level, WeaponType weaponType) {
        super(name, level);
        this.weaponType = weaponType;
    }



    @Override
    public void equip(Item item, Character character){
        Inventory inventory = character.getInventory();

        try{
            if(inventory.getHand().equals(item)){
                System.out.println("The item is already equipped");
                return;
            }
        }catch (NullPointerException ignored){}



        if(character.getLevelInstance().getLevel() >= item.getLevel()){
            swapItem(null, character);



            inventory.setHand(item);
            character.setDamage(weaponType.getFullDamage(item.getLevel(),character.getAttributes()));

            // This will make sure that the item is out of the bag.
            inventory.getItemFromInventory(item.getName());
        }else{
            System.out.println("The item is to high level");
        }

    }

    @Override
    public void swapItem(Item item, Character character){
        Inventory inventory = character.getInventory();
        if(inventory.getHand() != null){
            if(!inventory.inventoryContainsItem(inventory.getHand().getName())){
                inventory.putItemInInventory(inventory.getHand().getName(),inventory.getHand());
                character.setDamage(0);
            }
        }
    }

    @Override
    public void unequip(Item item, Character character) {

        Inventory inventory = character.getInventory();
        if(item.equals(inventory.getHand())){
            inventory.setHand(null);
        }else{
            System.out.println("Weapon is not equipped");
            return;
        }
        inventory.putItemInInventory(item.getName(),item);
        character.setDamage(0);
    }

    public WeaponType getWeaponType(){
        return this.weaponType;
    }
}
