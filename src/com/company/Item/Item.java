package com.company.Item;

public abstract class Item implements Equipable {
    private int level;
    private String name;

    public Item(String name, int level){
        this.name = name;
        this.level = level;
    }

    public String getName(){
        return this.name;
    }

    public int getLevel(){
        return this.level;
    }

}
