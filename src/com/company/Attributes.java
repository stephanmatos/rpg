package com.company;

public class Attributes {

    /*
    Slot 0 - Health
    Slot 1 - Strength
    Slot 2 - Dexterity
    Slot 3 - Intelligence
     */

    private int health;
    private int strength;
    private int dexterity;
    private int intelligence;

    // Attributes keeps track of the character and item attributes. Getters and setters to add and subtract
    // when new level gained and items equipped or unequipped
    public Attributes(int health, int strength, int dexterity, int intelligence){
        this.health = health;
        this.dexterity = dexterity;
        this.strength = strength;
        this.intelligence = intelligence;
    }

    public void setAllAttributes(Integer[] attributes){
        this.health = attributes[0];
        this.strength = attributes[1];
        this.dexterity = attributes[2];
        this.intelligence = attributes[3];
    }

    public Integer[] getAllAttributes(){
        return new Integer[]{this.health,this.strength,this.dexterity,this.intelligence};
    }

    public void addAllAttributes(Integer[] attributes){
        this.health += attributes[0];
        this.strength += attributes[1];
        this.dexterity += attributes[2];
        this.intelligence += attributes[3];
    }

    public void subtractAllAttributes(Integer[] attributes){
        this.health -= attributes[0];
        this.strength -= attributes[1];
        this.dexterity -= attributes[2];
        this.intelligence -= attributes[3];
    }


    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void addHealth(int health){
        this.health += health;
    }

    public void subtractHealth(int health){
        this.health -= health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void addStrength(int strength){
        this.strength += strength;
    }

    public void subtractStrength(int strength){
        this.strength -= strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void addDexterity(int dexterity){
        this.dexterity += dexterity;
    }

    public void subtractDexterity(int dexterity){
        this.dexterity -= dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void addIntelligence(int intelligence){
        this.intelligence += intelligence;
    }

    public void subtractIntelligence(int intelligence){
        this.intelligence -= intelligence;
    }

}
