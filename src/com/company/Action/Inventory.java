package com.company.Action;

import com.company.Item.Item;

import java.util.HashMap;

public class Inventory {

    // Hashmap for storing items not in use
    public HashMap<String, Item> inventory;

    // Four Items for the different body parts - Could be argued this should be an array.
    // But I dont see the need as long as the character only can carry 4 items at a time
    private Item head = null;
    private Item body = null;
    private Item legs = null;
    private Item hand = null;

    public Inventory(){
        // Initialize the hashmap when a character is created.
        inventory = new HashMap<>();
    }

    public void putItemInInventory(String name, Item item){

        // Put item in inventory - checks if the item is already there.
        // Hashmap can not contain two keys with the same name
        if(!inventory.containsKey(name)){
            inventory.put(name,item);
        }else{
            System.out.println("Error, Item is already in the inventory");
        }
    }

    // Checks if item is in bag, returns and removes it if true with the two next methods
    public Item getItemFromInventory(String name){

        if(inventoryContainsItem(name)){
            return removeItemFromInventory(name);
        }
        return null;
    }

    public Item removeItemFromInventory(String name){
        return inventory.remove(name);
    }

    public Boolean inventoryContainsItem(String name){
        return inventory.containsKey(name);
    }

    // Getters and setters
    public Item getBody() {
        return body;
    }
    public void setBody(Item body) {
        this.body = body;
    }
    public Item getHand() {
        return hand;
    }
    public void setHand(Item hand) {
        this.hand = hand;
    }
    public Item getLegs(){
        return this.legs;
    }
    public void setLegs(Item legs) {
        this.legs = legs;
    }
    public Item getHead(){
        return this.head;
    }
    public void setHead(Item head) {
        this.head = head;
    }
}
