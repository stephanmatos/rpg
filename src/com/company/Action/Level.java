package com.company.Action;

public class Level {

    // experience integers to hold current, target and the level experience modifier
    private int currentXP = 0;
    private int targetXP = 100;
    private int levelXP = 100;

    private int level = 1;


    public int getLevel(){
        return level;
    }

    public void gainXP(int xp){
        currentXP += xp;
    }

    public boolean checkXP(){
        return currentXP > targetXP;
    }

    public void gainLevel(){
        level++;
        System.out.println("You are now level "+level);
        levelXP = (int)(levelXP *1.1);

        targetXP = targetXP + levelXP;
    }

    public int getCurrentXP(){
        return this.currentXP;
    }

    public int getTargetXP(){
        return this.targetXP;
    }



}
