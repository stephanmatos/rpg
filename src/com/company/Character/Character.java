package com.company.Character;

import com.company.Action.Inventory;
import com.company.Action.Level;
import com.company.Attributes;
import com.company.Item.Item;
import com.company.Item.Weapon.Weapon;

import static com.company.Main.formatText;

public abstract class Character {

    private final Level level;
    protected final Attributes attributes;
    private String name;
    private final Inventory inventory = new Inventory();
    private int damage = 0;

    public Character(String name, Attributes attributes){
        this.name = name;
        this.attributes = attributes;
        level  = new Level();
    }
    public abstract void updateStats();


    public void printStats(){

        String first = "+***********************************+";
        System.out.println(first);

        formatText(first.length(),"| "+ name + " details:");
        formatText(first.length(),"| Class      : " + this.getClass().getSimpleName());
        formatText(first.length(),"| Damage     : " + damage);
        formatText(first.length(),"| HP         : " + this.attributes.getHealth());
        formatText(first.length(),"| Str        : " + attributes.getStrength());
        formatText(first.length(),"| Dex        : " + attributes.getDexterity());
        formatText(first.length(),"| Int        : " + attributes.getIntelligence());
        formatText(first.length(),"| Lvl        : " +level.getLevel());
        formatText(first.length(),"| Current xp : " +level.getCurrentXP());
        formatText(first.length(),"| Target  xp : " +level.getTargetXP());
        System.out.println("+***********************************+");

    }

    // Updates the damage. Used to recalculate when armor and weapons are equipped and unequipped
    public void updateDamage(){
        Item weapon = inventory.getHand();
        if(weapon != null){
            this.damage = ((Weapon) weapon).getWeaponType().getFullDamage(weapon.getLevel(),attributes);
        }else{
            this.damage = 0;
        }
    }

    // Getters and setters
    public void setDamage(int damage){
        this.damage = damage;
    }
    public int getDamage(){
        return this.damage;
    }
    public Level getLevelInstance(){
        return this.level;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public Inventory getInventory(){
        return this.inventory;
    }
    public Attributes getAttributes(){
        return this.attributes;
    }
}
