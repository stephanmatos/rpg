package com.company.Character;

import com.company.Attributes;

public class Warrior extends Character {

    public Warrior(String name){
        super(name,new Attributes(150,10,3,1));
    }

    public void updateStats(){
        this.attributes.addHealth(30);
        this.attributes.addStrength(5);
        this.attributes.addDexterity(2);
        this.attributes.addIntelligence(1);
    }

}
