package com.company.Character;

import com.company.Attributes;

public class Ranger extends Character {

    public Ranger(String name){
        super(name,new Attributes(120,5,10,2));
    }

    public void updateStats(){

        this.attributes.addHealth(20);
        this.attributes.addStrength(2);
        this.attributes.addDexterity(5);
        this.attributes.addIntelligence(1);
    }



}
