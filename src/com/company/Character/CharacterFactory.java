package com.company.Character;

public class CharacterFactory {

    // Factory to complete the pattern
    public Character getCharacter(CharacterTypes characterType, String name) {

        if (characterType == CharacterTypes.Warrior) {
            return new Warrior(name);
        } else if (characterType == CharacterTypes.Ranger) {
            return new Ranger(name);
        } else if (characterType == CharacterTypes.Mage) {
            return new Mage(name);
        }
        return null;
    }
}
