package com.company.Character;

import com.company.Attributes;

public class Mage extends Character {

    public Mage(String name){
        super(name,new Attributes(100,2,3,10));
    }

    public void updateStats(){
        this.attributes.addHealth(15);
        this.attributes.addStrength(1);
        this.attributes.addDexterity(2);
        this.attributes.addIntelligence(5);
    }

}
